J'ai initialisé la combinaison aléatoire à 1 1 1 1, afin de pouvoir tester la victoire, et voir si rejouer fonctionnait bien (ligne 69), j'ai malheureusement omis de la rechanger par la suite. 
Pour générer une combinaison aléatoire la formule est rand()%10, cette partie du code est donc fausse, à remplacer par :

for(i = 0; i < 4; i++)
            {
                MM.combinaison[i] = rand()%10;//initialisation de la combinaison aléatoire
            }