#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int i; //it�rateur
int j; //it�rateur

struct jeu
{
    int combinaison[4]; //combinaison des 4 couleurs
    int nbEssaisRestants; //nombre d'essais
    int partiesGagnees; //nombre de parties gagn�es
    int partiesPerdues; //nombre de parties perdues
};

struct verif
{
    int bienPlaces; //nombre de chiffres justes, au bon endroit
    int malPlaces; //nombre de chiffres justes, au mauvais endroit
};


struct verif testCombi (struct jeu MM, int combiJoueur[])
{
    struct verif placement;
    placement.bienPlaces = 0; //nombre de chiffres bien plac�s dans la combinaison du joueur
    placement.malPlaces = 0; //nombre de chiffres mal plac�s dans la combinaison du joueur
    for(i = 0; i < 4; i++)
    {
        if (MM.combinaison[i] == combiJoueur[i]) //on teste si le chiffre est bien plac�
        {
            placement.bienPlaces++; // on incr�mente de 1 le nombre de chiffres justes, au bon endroit
        }
        else
        {
            for(j = 0; j < 4; j++)
            {
                if ((MM.combinaison[i] == combiJoueur[j]) && (i != j)) //on test si le chiffre est mal plac�
                {
                    placement.malPlaces++; //on incr�ment de 1 le nombre de chiffres justes mais au mauvais endroit
                }
            }
        }
    }

    return placement;
}

int main()
{
    srand(time(NULL)); //initialisation hasard
    int combiJoueur[4]; //combinaison du joueur
    int choixCombiJoueur; //chiffre de joueur qu'il souhaitera mettre dans sa combinaison
    struct jeu MM;
    MM.nbEssaisRestants = 10; //nombre d'essais iniatilis� � 10
    MM.partiesGagnees = 0; //nombre de parties que le joueur a gagn�
    MM.partiesPerdues = 0; //nombre de parties que le joueur a perdu
    int jouer = 1; //pour v�rifier si le joueur veut rejouer, condition d'entree dans le jeu
    int k = 0;


    while (jouer ==1)
    {
        jouer = 0;
        do
        {
            for(i = 0; i < 4; i++)
            {
                MM.combinaison[i] = 1;//initialisation de la combinaison al�atoire
            }
        }
        while(k == 1); //pour ne faire la g�n�ration al�atoire de la combinaison qu'une fois par partie
        while ((MM.nbEssaisRestants > 0)) //tant qu'il reste des essais au joueur
        {
            printf("Vous avez %d essais.\n", MM.nbEssaisRestants);
            for(i = 0; i < 4; i++) //permet au joueur de saisier ses 4 chiffres dans sa combinaison
            {
                printf("Entrez votre combinaison : (entre 0 et 9)\n%de chiffre : \n", i+1);
                scanf("%d", &choixCombiJoueur);
                combiJoueur[i] = choixCombiJoueur; //saisie de la combinaison du joueur
            }

            printf("Vous avez saisi la combinaison : ");
            for(i = 0; i < 4; i++)
            {
                printf("%d ", combiJoueur[i]); //affichage de la combinaison saisie par le joueur
            }
            printf("\n");
            MM.nbEssaisRestants--; //on d�crement de 1 le nombre d'essais � chaque essai du joueur

            testCombi(MM, combiJoueur);
            printf("Nombre de bien places : %d\n", testCombi(MM, combiJoueur).bienPlaces); //affiche le nombre de chiffres justes, au bon endroit
            printf("Nombre de mal places : %d\n", testCombi(MM, combiJoueur).malPlaces); // nombre de chiffres justes, au mauvais endroit

            if (testCombi(MM, combiJoueur).bienPlaces == 4) //si le joueur a gagn�
            {
                MM.partiesGagnees++;
                printf("Vous gagnez la partie.\nVous avez gagne %d parties.\nVous avez perdu %d parties.\nVoulez vous rejouer ? (1/Oui, 2/Non)\n", MM.partiesGagnees, MM.partiesPerdues);
                scanf("%d", &jouer); //on demande au joueur s'il veut rejouer
                MM.nbEssaisRestants = 10;
                if (jouer != 1)
                {
                    return 0;
                }
            }
            else if (MM.nbEssaisRestants == 0) //si le joueur a perdu
            {
                MM.partiesPerdues++;
                printf("Vous perdez la partie.\nVous avez gagne %d parties.\nVous avez perdu %d parties.\nVoulez vous rejouer ? (1/Oui, 2/Non)\n", MM.partiesGagnees, MM.partiesPerdues);
                scanf("%d", &jouer); //on demande au joueur s'il veut rejouer
                MM.nbEssaisRestants = 10;
                if (jouer != 1)
                {
                    return 0;
                }
            }
        }
    }
    return 0;
}
